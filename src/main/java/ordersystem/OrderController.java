package ordersystem;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for REST API endpoints for Orders
 * 
 * @author Karl R. Wurst
 * Fall 2018
 */
@RestController
public class OrderController {

	private static final AtomicLong counter = Database.getOrderCounter();
    private static Map<Long, Order> orderDb = Database.getOrderDb();
    private static Map<Long, Product> productDb = Database.getProductDb();

    /**
     * Create a new order for a specific Customer
     * @param customerNumber the customer who is placing the order
     * @return the order number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/orders/new/{customerNumber}")
    public ResponseEntity<Long> addNewOrder(@PathVariable long customerNumber) {
    	Order order = new Order(customerNumber);
    	order.setNumber(counter.incrementAndGet());
    	orderDb.put(order.getNumber(), order);
    	return new ResponseEntity<>(order.getNumber(), HttpStatus.CREATED);
    }
    
    /**
     * Get the customer number for a specific order
     * 
     * @param number the order number
     * @return the customer number for that order
     */
    @GetMapping("/orders/{number}/customer")
    public ResponseEntity<Object> getCustomerNumber(@PathVariable long number) {
    	if (orderDb.containsKey(number)) {
            return new ResponseEntity<>(orderDb.get(number).getCustomerNumber(), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Order does not exist", HttpStatus.NOT_FOUND);
    	}
    }
    
    /**
     * Get the order lines for a specific order
     * @param number the order number
     * @return the order lines
     */
    @GetMapping("/orders/{number}/lines")
    public ResponseEntity<Object> getOrderLines(@PathVariable long number) {
    	if (orderDb.containsKey(number)) {
            return new ResponseEntity<>(orderDb.get(number).getLines(), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Order does not exist", HttpStatus.NOT_FOUND);
    	}
    } 
  
    /**
     * Add to the quantity of a product in this order
     * @param number the order number
     * @param line a line containing a SKU and quantity
     * @return successful if the SKU is in the order, not found if not
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PutMapping("/orders/{number}/lines")
    public ResponseEntity<String> addProductToOrder(@PathVariable long number, @RequestBody Line line) {
    	if (orderDb.containsKey(number)) {
    		orderDb.get(number).addToOrder(line);
    		return new ResponseEntity<>("Order successfully updated", HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Order does not exist", HttpStatus.NOT_FOUND);
    	}
    } 
 
    /**
     * Reduce the quantity of a product in this order. If the quantity becomes <= 0 remove the product line completely
     * @param number the order number
     * @param line a line containing a SKU and quantity
     * @return a line containing a SKU and quantity
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @DeleteMapping("/orders/{number}/lines")
    public ResponseEntity<String> removeProductFromOrder(@PathVariable long number, @RequestBody Line line) {
    	if (orderDb.containsKey(number)) {
    		if (orderDb.get(number).removeFromOrder(line)) {
    			return new ResponseEntity<>("Order successfully updated", HttpStatus.OK);
    		} else {
    			return new ResponseEntity<>("SKU does not exist", HttpStatus.NOT_FOUND);
    		}
    	} else {
    		return new ResponseEntity<>("Order does not exist", HttpStatus.NOT_FOUND);
    	}
    } 
    @GetMapping("/orders/{Cnumber}/check")
    public ResponseEntity<Object> getOrderNumbersByCustomerNumber(@PathVariable long Cnumber){
    	if(orderDb.containsKey(Cnumber)) {
    		long i = 1;
    		ArrayList <String> orders = new ArrayList<String> (orderDb.size()+1);
    		while(orderDb.get(i) != null) {
    			if (orderDb.get(i).getCustomerNumber() == Cnumber) {
    				orders.add(orderDb.get(i).toString());    			
    				}
    		i++;
    		}
    		return new ResponseEntity<>(orders, HttpStatus.OK);
    	}
    	else {
    		long notFound = -1;
    		return new ResponseEntity<>(notFound, HttpStatus.NOT_FOUND);
    	}
    }
   /* @GetMapping("/orders/{number}/orderForm")//not Finished
    public ResponseEntity<Object> getOrderForm(@PathVariable long number){
    	if(orderDb.containsKey(number)) {
    		long i = 1;
    		long total = 0;
    		ArrayList <String> orders = new ArrayList<String> (orderDb.size()+1);
    		while (orderDb.get(i) != null) {
    			if (orderDb.get(i).getNumber() == number) {
    				orders.add(orderDb.get(i).toString());
    				long priceForLine = productDb.get(i).getUnitPrice();
    				total += priceForLine;
    				//get unit price and quantity and store them in price for line
    				//add that number to total price which will be stored and printed at the end
    			}
    			i++;
    			}
    		return new ResponseEntity<>(orders, HttpStatus.OK);
    	}
    	else {
    		long notFound = -1;
    		return new ResponseEntity<>(notFound, HttpStatus.NOT_FOUND);
    	}
    }*/
    
}