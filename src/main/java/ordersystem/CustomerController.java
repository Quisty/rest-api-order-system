package ordersystem;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PutMapping;
/**
 * Controller for REST API endpoints for Customers
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
@RestController
public class CustomerController {

	private static final AtomicLong counter = Database.getCustomerCounter();
    private static Map<Long, Customer> customerDb = Database.getCustomerDb();
  
    /**
     * Create a new customer in the database
     * @param customer customer with first and last names
     * @return the customer number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/customers/new")
    public ResponseEntity<Long> addNewCustomer(@RequestBody Customer customer) {
    	customer.setNumber(counter.incrementAndGet());
    	customerDb.put(customer.getNumber(), customer);
    	return new ResponseEntity<>(customer.getNumber(), HttpStatus.CREATED);
    }
    
    /**
     * Get a customer from the database
     * @param number the customer number
     * @return the customer if in the database, not found if not
     */
    @GetMapping("/customers/{number}")
    public ResponseEntity<Object> getCustomerByNumber(@PathVariable long number) {
    	if (customerDb.containsKey(number)) {
            return new ResponseEntity<>(customerDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    	}
    } 
    @GetMapping("/customers/all")
    public ResponseEntity<Object> getAllCustomers(){
    	if(customerDb.isEmpty() == false) {
    	return new ResponseEntity<>(customerDb.values(), HttpStatus.OK);
    	}
    	else  {
    		return new ResponseEntity<>("no customers exist in database", HttpStatus.NOT_FOUND);
    	}
    }
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PutMapping("/customers/{number}/changeName")
    public ResponseEntity<String> changeName(@PathVariable long number,@RequestBody Customer customer ) {
    	if (customerDb.containsKey(number)) {
    		customer.setNumber(number);
    		customerDb.put(customer.getNumber(), customer);
    		return new ResponseEntity<>("Customer Name successfully updated", HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    	}
    }
    @GetMapping("/customers/{firstName}/{lastName}")
    public ResponseEntity<Object> getCustomerNumber(@PathVariable String firstName, @PathVariable String lastName){
    	if(customerDb.isEmpty() == false) {
    				long i = 1;
    				while(customerDb.get(i) != null) {
    					if((customerDb.get(i).getFirstName().equals(firstName)) && (customerDb.get(i).getLastName().equals(lastName))) {
    						return new ResponseEntity<>(i, HttpStatus.OK);
    					}
    					i++;
    				}
    			long notFound = -1;
        		return new ResponseEntity<>(notFound, HttpStatus.NOT_FOUND);
    	}
    	else  {
    		long notFound = -1;
    		return new ResponseEntity<>(notFound, HttpStatus.NOT_FOUND);
    		}
    	}
}
    

       

